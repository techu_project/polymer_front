var url = "http://localhost:3000/usuarios";

//Consultar cliente en backend
function obtenerUsuario(userId){
  var request = new XMLHttpRequest();
  try {
    request.open("GET",url + "/" + userId,false);
    request.setRequestHeader("Accept","application/json");
    request.send();
    var usuario = JSON.parse(request.responseText);
    return usuario;

  } catch (e) {
    console.log('Error al consultar usuario: ' + e.description);
    return null;
  }

}

//Registrar usuario
function registrarUsuario(userId, name, lastName, email, password){
  try{
    var request = new XMLHttpRequest();
    request.open("POST",url,false);
    request.setRequestHeader("Accept","application/json");
    request.setRequestHeader("Content-Type","application/json");
    var userNew = {"idcliente": userId, "nombre": name, "apellido": lastName ,"email": email, "password": password};
    var body = JSON.stringify(userNew);
    request.send(body);
    console.log('RTA creacion usuario:' + request.responseText);
    return(request.responseText);
  }catch(e){
    console.log('Error al insertar usuario en base de datos' + e.description);
  }
}
