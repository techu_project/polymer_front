var url = "http://localhost:3000/cuentas";

function cuentasCliente(userId){
  try{
    var request = new XMLHttpRequest();
    console.log('URL cuentas: ' + url + '/' + userId);
    request.open("GET",url + '/' + userId ,false);
    request.setRequestHeader("Accept","application/json");
    request.send();
    var cuentas = JSON.parse(request.responseText);
    console.log('Consulta cuentas: ' + cuentas);
    return(cuentas);
  }catch(e){
    console.log("Error en obtener Ctas cliente" + e.description);
  }
}

function insertarCuenta(tipoCuenta, idCliente){
  try {
    var request = new XMLHttpRequest();
    request.open("POST",url,false);
    request.setRequestHeader("Accept","application/json");
    request.setRequestHeader("Content-Type","application/json");
    var nuevaCuenta = {"idcliente": idCliente, "tipocuenta": tipoCuenta, "movimientos":[{"fecha": "27/08/2019","importe": 0,"categoria": "Crea Cta"}]};
    var body = JSON.stringify(nuevaCuenta);
    request.send(body);
    this.respuesta = request.responseText;
    console.log('RTA creacion cuenta: ' + this.respuesta);
  } catch (e) {
    console.log('Error al crear cuenta' + e.description);

  }
}
